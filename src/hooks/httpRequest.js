import { useState, useEffect } from 'react';

export const useHttp = (url, dependencies) => {
    const [isLoading, setIsLoading] = useState(false);
    const [fetchedData, setFetchedData] = useState(null);
    useEffect(() => {
        setIsLoading(true);
        fetch(url).then(response => { 
            if (!response.ok) {
                throw new Error (`Failed data fetching from: ${url}`);
            }
            return response.json();
        }).then(data => {
            console.log(data)
            setIsLoading(false);
            setFetchedData(data);
        }).catch(error => {
            console.log(error);
            setIsLoading(false);
        });
    }, dependencies);

    return [isLoading, fetchedData]
};