export const prestationsReglees = () => {
    const remboursement = [
        {
            id:1,
            name: "Hospitalisation",
            ResteACharge: 17496, 
            SecuSociale: 12200,
            Cogevie: 5245,
            presta: 0.06,
            exp: 0.15,
            couvRate: 0.997,
            couvExp: 0.996
        },
        {
            id:2,
            name: "Consultations et visites",
            ResteACharge: 25262, 
            SecuSociale: 16589,
            Cogevie: 7832,
            presta: 0.1,
            exp: 0.1,
            couvRate: 0.967,
            couvExp: 0.952
        },
        {
            id:3,
            name: "Soins courants",
            ResteACharge: 35505, 
            SecuSociale: 22625,
            Cogevie: 12806,
            presta: 0.16,
            exp: 0.11,
            couvRate: 0.998,
            couvExp: 0.991
        },
        {
            id:4,
            name: "Transport",
            ResteACharge: 88, 
            SecuSociale: 57,
            Cogevie: 31,
            presta: 0.01,
            exp: 0,
            couvRate: 1,
            couvExp: 0.987
        },
        {
            id:5,
            name: "Pharmacie",
            ResteACharge: 27745, 
            SecuSociale: 15893,
            Cogevie: 11848,
            presta: 0.15,
            exp: 0.12,
            couvRate: 1,
            couvExp: 0.993
        },
        {
            id:6,
            name: "Dentaire",
            ResteACharge: 29838, 
            SecuSociale: 8528,
            Cogevie: 19607,
            presta: 0.24,
            exp: 0.20,
            couvRate: 0.943,
            couvExp: 0.829
        },
        {
            id:7,
            name: "Optique",
            ResteACharge: 23345, 
            SecuSociale: 909,
            Cogevie: 17786,
            presta: 0.22,
            exp: 0.25,
            couvRate: 0.801,
            couvExp: 0.806
        },
        {
            id:8,
            name: "Autres prothèses et appareillages",
            ResteACharge: 7163, 
            SecuSociale: 3240,
            Cogevie: 3338,
            presta: 0.04,
            exp: 0.03,
            couvRate: 0.918,
            couvExp: 0.888
        },
        {
            id:9,
            name: "Medecine douce",
            ResteACharge: 3665, 
            SecuSociale: 1,
            Cogevie: 2661,
            presta: 0.03,
            exp: 0.04,
            couvRate: 0.726,
            couvExp: 0.782
        },
    ]
    return remboursement;
}