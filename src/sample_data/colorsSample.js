export const colorsSample = () => {
    const COLORS = [
        '#66ffff',
        '#ffccff',
        '#ffcc99',
        '#ccffcc',
        '#99ccff',
        '#ff99ff',
        '#ff9999',
        '#66ff33',
        '#0066ff',
        '#cc00ff',
        '#ff0066',
        '#669999',
    ];
    return COLORS;
}