import React from 'react';
import './App.css';
import Navigation from './components/Nav';
import Rechart from './components/Rechart';
import Victory from './components/Victory';
import Nivo from './components/Nivo';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import 'dotenv';
import Rates from './components/Rates';
function App() {
  return (
    <Router>
      <div className="App">
        <Navigation/>
        <Switch>
          <Route path="/" exact component={Rechart}/>
          <Route path="/victory" component={Victory}/>
          <Route path="/nivo" component={Nivo}/>
          <Route path="/rates" component={Rates}/>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
