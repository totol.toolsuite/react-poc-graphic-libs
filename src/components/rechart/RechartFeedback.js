import React from 'react';
import { Table } from 'react-bootstrap';

function RechartFeedBack() {

    let content = ( 
        <>
            <h3>Feedback</h3>
            <Table striped bordered hover>
                <thead>
                    <tr>
                        <th>#</th>
                        <th style={{backgroundColor: 'green'}}>Avantages</th>
                        <th style={{backgroundColor: 'red'}}>Inconvénients</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td>Rapide à prendre en main et à installer</td>
                        <td>Légendes natives pas suffisemment maléables</td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>Documentation suffisante avec des exemples</td>
                        <td>Utilise les life cycles hooks pré React 16.8, ce qui génère un warning console, une issue est ouverte depuis plusieurs mois et n'est pas résolue à ce jour
                             <p>https://github.com/recharts/recharts/issues/1835</p>
                        </td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>Plusieurs graphiques proposés, avec la possibilité d'en mixer plusieurs en un seul pour des graphiques plus complexes</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>Libertée de customisation sur le plan design</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>5</td>
                        <td>Légende, tooltip etc fournis dans des composants réutilisables et modifiables</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Bilan</td>
                        <td colSpan="2">Rapide à prendre en main, un peu customisable, des animations et des rendus natifs assez sympas, un nombre de graphiques et de possibilités assez limité mais peut être suffisant</td>
                    </tr>
                </tbody>
            </Table>
        </>
        )
    return content;
}

export default RechartFeedBack;