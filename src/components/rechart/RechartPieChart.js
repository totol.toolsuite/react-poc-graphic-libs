import React, { useState, useEffect } from 'react';
import { prestationsReglees } from '../../sample_data/prestationsReglees'
import { PieChart, Pie, Tooltip, Cell, Legend } from 'recharts';
import { Row, Col } from 'react-bootstrap';
import _ from 'lodash';
import { colorsSample } from '../../sample_data/colorsSample';

function RechartPieChart() {
    const fetchedData = prestationsReglees();
    const COLORS = colorsSample();
    const [filteredData, setFilteredData] = useState(fetchedData);
    const [shownData, setShownData] = useState(filteredData);


    let content = <p>Loading ...</p>

    const onClickFilter = (item) => {
        let tempArray = _.cloneDeep(filteredData);
        let index = _.findIndex(filteredData, x => x.id === item.id);
        tempArray[index].filtered = !tempArray[index].filtered;

        setFilteredData(tempArray);
    }

    useEffect(() => {
        let tempArray = _.cloneDeep(filteredData);
        tempArray = _.map(tempArray, (x, index) => {
            return {...x, filtered: false, color: COLORS[index]}
        })
        setFilteredData(tempArray);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    useEffect(() => {
        setShownData(_.filter(filteredData, x => !x.filtered));
    }, [filteredData]);



    let customLegend = 
    <ul className={"legend-style"}>
        {
            filteredData.map(
            (x, index) => 
            <li key={x.id} onClick={() => onClickFilter(x)}>
                <span className={"dot"} style={{backgroundColor: COLORS[index]}}></span
                ><span style={{color: x.filtered ? 'grey' : 'black'}}>{x.name}</span>
            </li>
            )
        }
    </ul>
    if (fetchedData) {
        content = ( 
        <>
            <Row>
                <Col md="auto">    
                    <h3>Vos prestations</h3>    
                    {customLegend}    
                    <PieChart width={400} height={400}>
                        <Pie dataKey="presta" isAnimationActive={true} data={shownData} display="filtered" cx={200} cy={200} outerRadius={80} fill="#8884d8" label >
                        {
                            shownData.map((entry, index) => {
                                return <Cell key={index} fill={entry.color} />;
                            })
                        }
                        </Pie>
                        <Tooltip />
                    </PieChart>
                </Col>
                <Col md="auto">
                    <h3>Notre experience</h3>        
                    <PieChart width={400} height={400}>
                        <Legend iconType="circle" height={100} width={400} align="center" verticalAlign="bottom"/>
                        <Pie paddingAngle={5} dataKey="exp" isAnimationActive={true} data={shownData} cx={200} cy={200} innerRadius={60} outerRadius={80} fill="#8884d8" label >
                        {
                            shownData.map((entry, index) => {
                                return <Cell key={index} fill={entry.color} />;
                            })
                        }
                        </Pie>
                        <Tooltip />
                    </PieChart>
                </Col>
                <Col md="auto"> 
                    <h3>Vos prestations/Notre experience</h3>          
                    <PieChart width={400} height={400}>
                        <Pie dataKey="presta" isAnimationActive={true} data={shownData} cx="50%" cy="50%" outerRadius={50} fill="#8884d8" >
                        {
                            shownData.map((entry, index) => {
                                return <Cell key={index} fill={entry.color} />;
                            })
                        }
                        </Pie>
                        <Tooltip/>
                        <Pie dataKey="exp" isAnimationActive={true} data={shownData} cx="50%" cy="50%" innerRadius={60} outerRadius={80} fill="#8884d8" >
                        {
                            shownData.map((entry, index) => {
                                return <Cell key={index} fill={entry.color} />;
                            })
                        }
                        </Pie>
                        <Tooltip/>
                    
                    </PieChart>
                </Col>
            </Row>
        </>
        )

    }
    return content;
}

export default RechartPieChart;