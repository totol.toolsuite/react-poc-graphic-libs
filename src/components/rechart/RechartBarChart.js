import React from 'react';
import { prestationsReglees } from '../../sample_data/prestationsReglees'
import { BarChart, Tooltip, Bar, XAxis, YAxis, CartesianGrid, Legend, LabelList } from 'recharts';
import { Row, Col } from 'react-bootstrap';

function RechartBarChart() {
    const fetchedData = prestationsReglees();

    let content = <p>Loading ...</p>

 
    if (fetchedData) {
        content = ( 
        <>
            <Row>
                <Col md="auto">    
                    <h3>Vertical stacked bar chart</h3>
                    <BarChart margin={{top: 5, right: 30, left: 50, bottom: 5}} width={1500} height={500} data={fetchedData}>
                        <CartesianGrid strokeDasharray="3 3" />
                        <XAxis dataKey="name" interval={0} orientation="top" />
                        <YAxis />
                        <Tooltip />
                        <Legend />
                        <Bar dataKey="ResteACharge" fill="#ffccff" stackId="x"/>
                        <Bar dataKey="SecuSociale" fill="#ff99ff" stackId="x"/>
                        <Bar dataKey="Cogevie" fill="#cc00ff" stackId="x"/>
                    </BarChart>
                </Col>
            </Row>
            <Row>
                <Col md="auto">  
                    <h3>Horizontal stacked bar chart</h3>
                    <BarChart margin={{top: 5, right: 30, left: 50, bottom: 5}} width={1500} height={500} data={fetchedData} layout="vertical">
                        <CartesianGrid strokeDasharray="3 3" />
                        <XAxis type="number"/>
                        <YAxis type="category" dataKey="name"/>
                        <Tooltip />
                        <Legend />
                        <Bar dataKey="ResteACharge" fill="#ffccff" stackId="y">
                            <LabelList dataKey="ResteACharge" />
                        </Bar>
                        <Bar dataKey="SecuSociale" fill="#ff99ff" stackId="y">
                            <LabelList dataKey="SecuSociale" />
                        </Bar>
                        <Bar dataKey="Cogevie" fill="#cc00ff" stackId="y">
                            <LabelList dataKey="Cogevie" />
                        </Bar>
                    </BarChart>
                </Col>
            </Row>
            <Row>
                <Col md="auto">  
                    <h3>Horizontal bar chart</h3>
                    <BarChart margin={{top: 5, right: 30, left: 50, bottom: 5}} width={1500} height={500} data={fetchedData} layout="vertical">
                        <CartesianGrid strokeDasharray="3 3" />
                        <XAxis type="number"/>
                        <YAxis type="category" dataKey="name"/>
                        <Tooltip />
                        <Legend />
                        <Bar dataKey="ResteACharge" fill="#ffccff"/>
                        <Bar dataKey="SecuSociale" fill="#ff99ff"/>
                        <Bar dataKey="Cogevie" fill="#cc00ff"/>
                    </BarChart>
                </Col>
            </Row>
        </>
        )

    }
    return content;
}

export default RechartBarChart;