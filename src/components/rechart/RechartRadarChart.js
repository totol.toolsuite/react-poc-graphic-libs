import React from 'react';
import { prestationsReglees } from '../../sample_data/prestationsReglees'
import { RadarChart, PolarGrid, PolarAngleAxis, PolarRadiusAxis, Radar} from 'recharts';
import { Row, Col } from 'react-bootstrap';

function RechartRadarChart() {
    const fetchedData = prestationsReglees();
    let content = <p>Loading ...</p>

 
    if (fetchedData) {
        content = ( 
        <>
            <Row>
                <Col md="auto">    
                    <h3>Couverture rate radar chart</h3>
                    <RadarChart cx={400} cy={250} outerRadius={150} width={800} height={500} data={fetchedData}>
                        <PolarGrid />
                        <PolarAngleAxis dataKey="name" />
                        <PolarRadiusAxis />
                        <Radar dataKey="couvRate" stroke="#8884d8" fill="#8884d8" fillOpacity={0.8} label />
                    </RadarChart>
                </Col>
                <Col md="auto">    
                    <h3>Couverture rate combined radar chart</h3>
                    <RadarChart cx={400} cy={250} outerRadius={150} width={800} height={500} data={fetchedData}>
                        <PolarGrid />
                        <PolarAngleAxis dataKey="name" />
                        <PolarRadiusAxis />
                        <Radar dataKey="couvRate" stroke="#8884d8" fill="#8884d8" fillOpacity={0.3} />
                        <Radar dataKey="couvExp" stroke="#cc00ff" fill="#cc00ff" fillOpacity={0.3} />
                    </RadarChart>
                </Col>
            </Row>
            
        </>
        )

    }
    return content;
}

export default RechartRadarChart;