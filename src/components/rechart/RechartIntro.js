import React from 'react';

function RechartIntro() {

    let content = ( 
        <>
            <h3>Intro</h3>
            <p>Librairie construite à partir de modules D3</p>
            <p>Complètement livré sous forme de composants React</p>
            <p>Les composants sont clairement séparés en composants, la grille, le graphique, les barres, la légende etc ...</p>
            <p>Open source, gratuit.</p>
        </>
        )
    return content;
}

export default RechartIntro;