import React from 'react';
import '../App.css';
import { Link } from 'react-router-dom';
import { Navbar, Nav} from 'react-bootstrap';

function Navigation() {
    return (
        <Navbar bg="dark" variant="dark">
        <Navbar.Brand href="/">Poc librairies graphiques react</Navbar.Brand>
        <Nav className="mr-auto">
                <Nav.Link as={Link} to='/'>
                    Rechart
                </Nav.Link>
                <Nav.Link as={Link} to='/victory'>
                    Victory
                </Nav.Link>
                <Nav.Link as={Link} to='/nivo'>
                    Nivo
                </Nav.Link>
                <Nav.Link as={Link} to='/rates'>
                    Rates
                </Nav.Link>
        </Nav>
        </Navbar>
    );
}

export default Navigation;