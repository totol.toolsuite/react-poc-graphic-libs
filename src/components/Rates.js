import React from 'react';
import { Table } from 'react-bootstrap';

function Rates() {
    return (
        <div style={{margin: '50px'}}>
            <h3>
                Retour
            </h3>
            <Table striped bordered hover>
                <thead>
                    <tr>
                        <th>Notes sur 10</th>
                        <th>Rechart</th>
                        <th>Victory</th>
                        <th>Nivo</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Installation</td>
                        <td>10</td>
                        <td>10</td>
                        <td>10</td>
                    </tr>
                    <tr>
                        <td>Prise en main / Simplicité</td>
                        <td>9</td>
                        <td>6</td>
                        <td>8</td>
                    </tr>
                    <tr>
                        <td>Documentation</td>
                        <td>8</td>
                        <td>6</td>
                        <td>11</td>
                    </tr>
                    <tr>
                        <td>Personnalisation</td>
                        <td>7</td>
                        <td>8</td>
                        <td>10</td>
                    </tr>
                    <tr>
                        <td>Différents types de graphiques</td>
                        <td>5</td>
                        <td>8</td>
                        <td>10</td>
                    </tr>
                    <tr>
                        <td>Esthétique (Attention, champs subjectif)</td>
                        <td>7</td>
                        <td>6</td>
                        <td>10</td>
                    </tr>
                    <tr>
                        <td>Testabilité ?</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                    </tr>
                    <tr>
                        <td style={{backgroundColor: 'gray'}}>Global</td>
                        <td>7</td>
                        <td>6</td>
                        <td>9</td>
                    </tr>
                </tbody>
            </Table>    
        </div>
        );
}

export default Rates;