import React from 'react';
import VictoryPieChart from './victory/VictoryPieChart';
import VictoryBarChart from './victory/VictoryBarChart';
import VictoryFeedBack from './victory/VictoryFeedback';
import VictoryIntro from './victory/VictoryIntro';

function Lib2() {
    return (
        <>
            <VictoryIntro/>
            <hr/>
            <VictoryPieChart/>
            <hr/>
            <VictoryBarChart/>
            <hr/>
            <VictoryFeedBack/>
        </>
    );
}

export default Lib2;