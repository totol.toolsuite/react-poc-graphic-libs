import React from 'react';
import RechartPieChart from './rechart/RechartPieChart'
import RechartBarChart from './rechart/RechartBarChart'
import RechartRadarChart from './rechart/RechartRadarChart'
import RechartFeedBack from './rechart/RechartFeedback';
import RechartIntro from './rechart/RechartIntro';

function Rechart() {
    return (
    <>
        <RechartIntro/>
        <hr/>
        <RechartPieChart/>
        <hr/>
        <RechartBarChart/>
        <hr/>
        <RechartRadarChart/>
        <hr/>
        <RechartFeedBack/>
    </>)
};
export default Rechart;