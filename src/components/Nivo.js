import React from 'react';
import NivoIntro from './nivo/NivoIntro';
import NivoPieChart from './nivo/NivoPieChart';
import NivoFeedback from './nivo/NivoFeedback';
import NivoBarChart from './nivo/NivoBarChart';
import NivoRadarChart from './nivo/NivoRadarChart';

function Nivo() {
    return (
        <>
            <NivoIntro/>
            <hr/>
            <NivoPieChart/>
            <hr/>
            <NivoBarChart/>
            <hr/>
            <NivoRadarChart/>
            <hr/>
            <NivoFeedback/>
        </>
    );
}

export default Nivo;