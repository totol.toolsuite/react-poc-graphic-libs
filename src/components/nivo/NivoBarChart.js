import React, { useState } from 'react';
import { prestationsReglees } from '../../sample_data/prestationsReglees'
import { ResponsiveBar } from '@nivo/bar';
import { Row, Col, Button } from 'react-bootstrap';

function NivoBarChart() {
    const fetchedData = prestationsReglees();
    
    const [reworkedData, setReworkedData] = useState(fetchedData);
    

    const rerender = () => {
        setReworkedData([]);
        setTimeout(() => setReworkedData(fetchedData), 500);
    }

    let content =
        ( 
        <>
            <Row>
                <Col>    
                    <h3>Bar chart</h3>  
                    <div style={{ margin: '30px', width: '1300px', height: '500px' }}>
                        <ResponsiveBar
                            data={reworkedData}
                            keys={['ResteACharge', 'SecuSociale', 'Cogevie']}
                            indexBy="name"
                            margin={{ top: 50, right: 130, bottom: 50, left: 60 }}
                            padding={0.3}
                            colors={{ scheme: 'nivo' }}
                            borderColor={{ from: 'color', modifiers: [ [ 'darker', 1.6 ] ] }}
                            axisTop={null}
                            axisRight={null}
                            labelSkipWidth={12}
                            labelSkipHeight={12}
                            labelTextColor={{ from: 'color', modifiers: [ [ 'darker', 1.6 ] ] }}
                            fill={[
                                {
                                    match: {
                                        id: 'ResteACharge'
                                    },
                                    id: 'dots'
                                },
                                {
                                    match: {
                                        id: 'Cogevie'
                                    },
                                    id: 'lines'
                                }
                            ]}
                            defs={[
                                {
                                    id: 'dots',
                                    type: 'patternDots',
                                    background: 'inherit',
                                    color: '#38bcb2',
                                    size: 4,
                                    padding: 1,
                                    stagger: true
                                },
                                {
                                    id: 'lines',
                                    type: 'patternLines',
                                    background: 'inherit',
                                    color: '#eed312',
                                    rotation: -45,
                                    lineWidth: 6,
                                    spacing: 10
                                }
                            ]}
                            legends={[
                                {
                                    dataFrom: 'keys',
                                    anchor: 'bottom-right',
                                    direction: 'column',
                                    justify: false,
                                    translateX: 120,
                                    translateY: 0,
                                    itemsSpacing: 2,
                                    itemWidth: 100,
                                    itemHeight: 20,
                                    itemDirection: 'left-to-right',
                                    itemOpacity: 0.85,
                                    symbolSize: 20,
                                    effects: [
                                        {
                                            on: 'hover',
                                            style: {
                                                itemOpacity: 1
                                            }
                                        }
                                    ]
                                }
                            ]}
                            animate={true}
                            motionStiffness={90}
                            motionDamping={15}
                        />
                    </div>
                </Col>
                <Col>    
                    <h3>Bar chart</h3> 
                    <Button onClick={() => rerender()}>Re render</Button> 
                    <div style={{ margin: '30px', width: '1300px', height: '500px' }}>
                        <ResponsiveBar
                            data={reworkedData}
                            keys={['ResteACharge', 'SecuSociale', 'Cogevie']}
                            indexBy="name"
                            margin={{ top: 50, right: 130, bottom: 50, left: 200 }}
                            padding={0.3}
                            colors={{ scheme: 'nivo' }}
                            borderColor={{ from: 'color', modifiers: [ [ 'darker', 1.6 ] ] }}
                            axisTop={null}
                            axisRight={null}
                            labelSkipWidth={12}
                            labelSkipHeight={12}
                            labelTextColor={{ from: 'color', modifiers: [ [ 'darker', 1.6 ] ] }}
                            fill={[
                                {
                                    match: {
                                        id: 'ResteACharge'
                                    },
                                    id: 'dots'
                                },
                                {
                                    match: {
                                        id: 'Cogevie'
                                    },
                                    id: 'lines'
                                }
                            ]}
                            defs={[
                                {
                                    id: 'dots',
                                    type: 'patternDots',
                                    background: 'inherit',
                                    color: '#38bcb2',
                                    size: 4,
                                    padding: 1,
                                    stagger: true
                                },
                                {
                                    id: 'lines',
                                    type: 'patternLines',
                                    background: 'inherit',
                                    color: '#eed312',
                                    rotation: -45,
                                    lineWidth: 6,
                                    spacing: 10
                                }
                            ]}
                            layout='horizontal'
                            legends={[
                                {
                                    dataFrom: 'keys',
                                    anchor: 'top',
                                    direction: 'row',
                                    justify: false,
                                    translateX: 0,
                                    translateY: -30,
                                    itemsSpacing: 2,
                                    itemWidth: 100,
                                    itemHeight: 20,
                                    itemDirection: 'left-to-right',
                                    itemOpacity: 0.85,
                                    symbolSize: 20,
                                    effects: [
                                        {
                                            on: 'hover',
                                            style: {
                                                itemOpacity: 1
                                            }
                                        }
                                    ]
                                }
                            ]}
                            animate={true}
                            motionStiffness={90}
                            motionDamping={15}
                        />
                    </div>
                </Col>
            </Row>
        </>
        )
    return content;
}

export default NivoBarChart;