import React, { useState } from 'react';
import { prestationsReglees } from '../../sample_data/prestationsReglees'
import { ResponsiveRadar } from '@nivo/radar';
import { Row, Col } from 'react-bootstrap';

function NivoRadarChart() {
    const fetchedData = prestationsReglees();
    
    const [reworkedData] = useState(fetchedData);
    

    let content =
        ( 
        <>
            <Row>
                <Col>    
                    <h3>Radar chart</h3>
                    <div style={{ margin: '30px', width: '500px', height: '500px' }}>
                        <ResponsiveRadar
                            data={reworkedData}
                            keys={[ 'couvRate', 'couvExp' ]}
                            indexBy="name"
                            maxValue="auto"
                            margin={{ top: 70, right: 80, bottom: 40, left: 80 }}
                            curve="linearClosed"
                            borderWidth={2}
                            borderColor={{ from: 'color' }}
                            gridLevels={8}
                            gridShape="circular"
                            gridLabelOffset={36}
                            enableDots={true}
                            dotSize={10}
                            dotColor={{ theme: 'background' }}
                            dotBorderWidth={2}
                            dotBorderColor={{ from: 'color' }}
                            enableDotLabel={true}
                            dotLabel="value"
                            dotLabelYOffset={-12}
                            colors={{ scheme: 'nivo' }}
                            fillOpacity={0.25}
                            animate={true}
                            motionStiffness={90}
                            motionDamping={15}
                            isInteractive={true}
                            legends={[
                                {
                                    anchor: 'top-left',
                                    direction: 'column',
                                    translateX: -50,
                                    translateY: -40,
                                    itemWidth: 80,
                                    itemHeight: 20,
                                    itemTextColor: '#999',
                                    symbolSize: 12,
                                    symbolShape: 'circle',
                                    effects: [
                                        {
                                            on: 'hover',
                                            style: {
                                                itemTextColor: '#000'
                                            }
                                        }
                                    ]
                                }
                            ]}
                        />
                    </div>
                </Col>
            </Row>
        </>
        )
    return content;
}

export default NivoRadarChart;