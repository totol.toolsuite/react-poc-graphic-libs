import React, { useState, useEffect } from 'react';
import { prestationsReglees } from '../../sample_data/prestationsReglees'
import { ResponsivePie } from '@nivo/pie'
import { Row, Col } from 'react-bootstrap';
import _ from 'lodash';

function NivoPieChart() {
    const fetchedData = prestationsReglees();
    
    const [reworkedData, setReworkedData] = useState(fetchedData);
    const [firstLoad, setFirstLoad] = useState(false);

    const replaceKey = (x, oldKey, newKey) => {
        if (oldKey !== newKey && x[oldKey]) {
            Object.defineProperty(x, newKey,
                Object.getOwnPropertyDescriptor(x, oldKey));
            delete x[oldKey];
        }
    }


    useEffect(() => {
        let tempData = _.map(_.cloneDeep(fetchedData), (item, index) => {
            item.id = item.name;
            item.filtered = false;
            replaceKey(item, 'name', 'label');
            replaceKey(item, 'presta', 'value');
            return item;
        })
        setReworkedData(tempData);
        setFirstLoad(true);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])
    
    let content = <h3>Loading</h3>
    if(firstLoad) {
        content =
        ( 
        <>
            <Row>
                <Col>    
                    <h3>Vos prestations</h3>  
                    <div style={{ margin: '30px', width: '500px', height: '500px' }}>
                        <ResponsivePie
                            data={reworkedData}
                            width={900}
                            height={500}
                            margin={{ top: 50, right: 50, bottom: 50, left: 50 }}
                            innerRadius={0.5}
                            padAngle={0.7}
                            cornerRadius={3}
                            colors={{ scheme: 'paired' }}
                            borderWidth={1}
                            borderColor={{ from: 'color', modifiers: [ [ 'darker', 0.2 ] ] }}
                            radialLabelsSkipAngle={10}
                            radialLabelsTextXOffset={6}
                            radialLabelsLinkOffset={0}
                            radialLabelsLinkDiagonalLength={16}
                            radialLabelsLinkHorizontalLength={24}
                            radialLabelsLinkStrokeWidth={1}
                            radialLabelsLinkColor={{ from: 'color' }}
                            slicesLabelsSkipAngle={10}
                            motionStiffness={90}
                            motionDamping={15}
                            legends={[
                                {
                                    anchor: 'left',
                                    direction: 'column',
                                    translateY: 56,
                                    itemWidth: 130,
                                    itemHeight: 30,
                                    itemTextColor: '#999',
                                    symbolSize: 18,
                                    symbolShape: 'circle',
                                    effects: [
                                        {
                                            on: 'hover',
                                            style: {
                                                itemTextColor: '#000'
                                            }
                                        }
                                    ]
                                }
                            ]}
                        />  
                    </div>
                </Col>
            </Row>
        </>
        )
    }
    return content;
}

export default NivoPieChart;