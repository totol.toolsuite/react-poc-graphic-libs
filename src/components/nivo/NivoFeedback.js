import React from 'react';
import { Table } from 'react-bootstrap';

function VictoryFeedBack() {

    let content = ( 
        <>
            <h3>Feedback</h3>
            <Table striped bordered hover>
                <thead>
                    <tr>
                        <th>#</th>
                        <th style={{backgroundColor: 'green'}}>Avantages</th>
                        <th style={{backgroundColor: 'red'}}>Inconvénients</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td>Possibilitées infinies</td>
                        <td>Doit-il y en avoir absolument ?</td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>Une documentation géniale et interractive, clairement au dessus du lot</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>Des storybooks fournis avec</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Bilan</td>
                        <td colSpan="2">Surement un peu plus long en place car beaucoup plus complet, mais peut avoir un super rendu. Gros gros + sur la documentation</td>
                    </tr>
                </tbody>
            </Table>
        </>
        )
    return content;
}

export default VictoryFeedBack;