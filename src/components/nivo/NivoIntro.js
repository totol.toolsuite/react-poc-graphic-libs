import React from 'react';

function NivoIntro() {

    let content = ( 
        <>
            <h3>Intro</h3>
            <p>Construit à partir de d3</p>
            <p>Une documentation clairement au dessus du lot</p>
            <p>Communauté active</p>
            <p>Open source, gratuit.</p>
        </>
        )
    return content;
}

export default NivoIntro;