import React, {useState, useEffect} from 'react';
import { prestationsReglees } from '../../sample_data/prestationsReglees'
import { Row, Col } from 'react-bootstrap';
import { VictoryBar, VictoryStack, VictoryLabel, VictoryTheme, VictoryTooltip, VictoryChart, VictoryAxis } from "victory";
import _ from 'lodash';

function CustomLabelComponent(props) {
    const text = props.datum.y > 100 ? `${props.datum.y}€` : '';
    return (
      <g>
        <VictoryLabel
          {...props}
          textAnchor="middle"
          verticalAnchor="middle"
          text={text}
          angle={0}
          dy={0}
          dx={0}
          style={{
            fontSize: 10,
            fill: 'black',
          }}
          renderInPortal={true}
        />
      </g>
    );
  }
  
function VictoryBarChart() {
    const fetchedData = prestationsReglees();
    
    const [reworkedDataRag, setReworkedDataRag] = useState(fetchedData);
    const [reworkedDataSs, setReworkedDataSs] = useState(fetchedData);
    const [reworkedDataCog, setReworkedDataCog] = useState(fetchedData);

    const [firstLoad, setFirstLoad] = useState(false);
    const replaceKey = (x, oldKey, newKey) => {
        if (oldKey !== newKey && x[oldKey]) {
            Object.defineProperty(x, newKey,
                Object.getOwnPropertyDescriptor(x, oldKey));
            delete x[oldKey];
        }
    }

    useEffect(() => {
        let tempDataRag = _.map(_.cloneDeep(fetchedData), (item, index) => {
            replaceKey(item, 'name', 'x');
            replaceKey(item, 'ResteACharge', 'y');
            item.label = item.y;
            return item;
        })
        setReworkedDataRag(tempDataRag);
        let tempDataCog = _.map(_.cloneDeep(fetchedData), (item, index) => {
            replaceKey(item, 'name', 'x');
            replaceKey(item, 'SecuSociale', 'y');
            item.label = item.y;
            return item;
        })
        setReworkedDataCog(tempDataCog);
        let tempDataSs = _.map(_.cloneDeep(fetchedData), (item, index) => {
            replaceKey(item, 'name', 'x');
            replaceKey(item, 'Cogevie', 'y');
            item.label = item.y;
            return item;
        })
        setReworkedDataSs(tempDataSs);
        setFirstLoad(true);
    }, [])

    let content = <h3>Loading</h3>;
    if (firstLoad) {
        content = 
            <>
                <Row>
                    <Col md="auto">    
                        <h3>Bar chart</h3>
                        <VictoryChart
                            height={400}
                            padding={{
                                left: 50, top: 20, bottom: 40, right: 20,
                            }}
                            theme={VictoryTheme.material}
                        >
                            <VictoryAxis
                            dependentAxis/>
                            <VictoryAxis 
                                style={{tickLabels: {marginTop: 25, angle: -45}}}
                            />
                            <VictoryStack 
                            labelComponent={<VictoryTooltip/>}
                            theme={VictoryTheme.material}
                            animate={{
                                duration: 2000,
                                onLoad: { duration: 1000 }
                            }}>
                                <VictoryBar style={{ data: { fill: "#c43a31" } }} data={reworkedDataRag} />
                                <VictoryBar data={reworkedDataSs} />
                                <VictoryBar data={reworkedDataCog} />
                            </VictoryStack>
                        </VictoryChart>
                    </Col>

                    <Col md="auto">    
                        <h3>Bar chart</h3>
                            <VictoryStack 
                            labelComponent={<CustomLabelComponent/>}
                            theme={VictoryTheme.material}
                            animate={{
                                duration: 2000,
                                onLoad: { duration: 1000 }
                            }}>
                                <VictoryBar style={{ data: { fill: "#c43a31" } }} data={fetchedData} y="ResteACharge" x="name"/>
                                <VictoryBar data={fetchedData} y="SecuSociale" x="name"/>
                                <VictoryBar data={fetchedData} y="Cogevie" x="name"/>
                            </VictoryStack>
                    </Col>
                </Row>
            </>
        } 

    return content;
}

export default VictoryBarChart;