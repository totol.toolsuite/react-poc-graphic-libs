import React from 'react';

function VictoryIntro() {

    let content = ( 
        <>
            <h3>Intro</h3>
            <p>Développé par FormidableLabs: open source spécialisés dans le React et l'ecosystem js</p>
            <p>Complètement livré sous forme de composants React</p>
            <p>Composants séparés comme Rechart</p>
            <p>Compatible React 15+</p>
            <p>Communauté très active</p>
        </>
        )
    return content;
}

export default VictoryIntro;