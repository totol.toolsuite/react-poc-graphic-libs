import React from 'react';
import { Table } from 'react-bootstrap';

function VictoryFeedBack() {

    let content = ( 
        <>
            <h3>Feedback</h3>
            <Table striped bordered hover>
                <thead>
                    <tr>
                        <th>#</th>
                        <th style={{backgroundColor: 'green'}}>Avantages</th>
                        <th style={{backgroundColor: 'red'}}>Inconvénients</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td>Installation simple</td>
                        <td>Documentation parfois imparfaite, manque d'exemples ou de détails</td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>Ultra personnalisable</td>
                        <td>Plus manuel, moins d'animations et de rendus natifs sympa que Rechart</td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>Beaucoup de possibilités</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Bilan</td>
                        <td colSpan="2">Un peu moins sumple à prendre en main et agréable que Rechart, dipose de plus de possibilités mais un peu plus long à mettre en place</td>
                    </tr>
                </tbody>
            </Table>
        </>
        )
    return content;
}

export default VictoryFeedBack;