import React, {useState, useEffect} from 'react';
import { prestationsReglees } from '../../sample_data/prestationsReglees'
import { colorsSample } from '../../sample_data/colorsSample'
import { Row, Col } from 'react-bootstrap';
import { VictoryPie } from "victory";
import _ from 'lodash';


function RechartPieChart() {
    const fetchedData = prestationsReglees();
    const COLORS = colorsSample();
    const [reworkedData1, setReworkedData1] = useState(fetchedData);
    const [firstLoad, setFirstLoad] = useState(false)
    const [shownData, setShownData] = useState(reworkedData1);

    const replaceKey = (x, oldKey, newKey) => {
        if (oldKey !== newKey && x[oldKey]) {
            Object.defineProperty(x, newKey,
                Object.getOwnPropertyDescriptor(x, oldKey));
            delete x[oldKey];
        }
    }

    const truncateString = (str, num) =>{
        if (str.length <= num) {
          return str
        }
        return str.slice(0, num) + '...'
    }

    useEffect(() => {
        let tempData = _.map(_.cloneDeep(fetchedData), (item, index) => {
            item.label = item.name;
            item.filtered = false;
            item.color = COLORS[index];
            replaceKey(item, 'name', 'x');
            replaceKey(item, 'presta', 'y');
            return item;
        })
        setReworkedData1(tempData);
        setShownData(tempData);
        setFirstLoad(true);
    }, [])

    useEffect(() => {
        setShownData(_.filter(reworkedData1, x => !x.filtered));
    }, [reworkedData1]);

    const onClickFilter = (item) => {
        let tempArray = _.cloneDeep(reworkedData1);
        let index = _.findIndex(reworkedData1, x => x.id === item.id);
        tempArray[index].filtered = !tempArray[index].filtered;
        setReworkedData1(tempArray);
    }
    const getColor = (x) => {
        return x.datum.color;
    }

    let customLegend = (
        <ul className={"legend-style"}>
            {
                reworkedData1.map(
                (x, index) => 
                <li key={x.id} onClick={() => onClickFilter(x)}>
                    <span className={"dot"} style={{backgroundColor: COLORS[index]}}></span
                    ><span style={{color: x.filtered ? 'grey' : 'black'}}>{x.label}</span>
                </li>
                )
            }
        </ul>
    )

    let content = <h3>Loading ....</h3>;
    if (firstLoad === true) {
        content = ( 
        <>
            <Row>
                <Col md="auto">    
                    <h3>Vos prestations</h3>
                    {customLegend}
                    <VictoryPie data={shownData} 
                      style={{
                        data: { fill: x => getColor(x) } 
                      }}/>
                </Col>
                <Col md="auto">    
                    <h3>Vos prestations</h3>
                    <VictoryPie data={reworkedData1} colorScale={COLORS} innerRadius={80} labelRadius={95}
                        padAngle={3}
                        labels={x => truncateString(x.datum.xName, 8)} style={{ labels: { fill: "white", fontSize: 10, fontWeight: "bold" } }}/>
                </Col>
            </Row>
        </>
        )

    }
    return content;
}

export default RechartPieChart;